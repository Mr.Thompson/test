<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\{
    IndexRequest,
    CreateRequest,
    StoreRequest
};
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(IndexRequest $request)
    {
        $order = $request->all() ? $request->all()['order'] : 'desc';
        $column = $request->all() ? $request->all()['column'] : 'id';
        $more = $request->all() ? ($request->all()['more'] === 'true' ? true : false ) : true;
        $user_count = $request->all() ? count($request->all()['users']) : 0;
        $users = User::where('isActive', 1)
            ->orderBy($column, $order)
            ->get();
        $count_for_output = $more ? 2 + $user_count : $user_count;
        $active = $count_for_output >= $users->count() ? false : true;
        $response = [
            'users' => $users->take($count_for_output),
            'active' => $active,
            'more' => false,
            'column' => $column,
            'order' => $order
        ];
        if ($request->ajax()) {
            return response()->json($response);
        }
        return view('users.index', $response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(CreateRequest $request)
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $data = json_decode($request->getContent(), true);
            foreach ($data as $item) {
                foreach ($item as $users) {
                    foreach ($users as $user) {
                        $response_user = User::create([
                            'isActive' => $user['isActive'],
                            'age' => $user['age'],
                            'eyeColor' => $user['eyeColor'],
                            'name' => $user['name'],
                            'gender' => $user['gender'],
                            'company' => $user['company'],
                            'email' => $user['email'],
                            'phone' => $user['phone'],
                            'address' => $user['address'],
                            'password' => Hash::make(123123123)
                        ]);
                    }
                }
            }
            return response()->json(['data' => $response_user], 201);
        }
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
