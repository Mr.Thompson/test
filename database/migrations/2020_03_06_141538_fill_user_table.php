<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class FillUserTable extends Migration
{
    protected $users = [
        [
            'age' => 25,
            'eyeColor' => 'light brown',
            'name' => 'Admin',
            'gender' => 'male',
            'company' => 'Test',
            'email' => 'admin@mr.th',
            'phone' => '+77777777777',
            'address' => 'www.test.com',

        ],
        [
            'age' => 23,
            'eyeColor' => 'light brown',
            'name' => 'Mr.Thompson',
            'gender' => 'male',
            'company' => 'Test',
            'email' => 'mr_thompson@mr.th',
            'phone' => '+78888888888',
            'address' => 'www.test.com',

        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->users as $user) {
            User::create([
                'age' => $user['age'],
                'eyeColor' => $user['eyeColor'],
                'name' => $user['name'],
                'gender' => $user['gender'],
                'company' => $user['company'],
                'email' => $user['email'],
                'email_verified_at' => \Carbon\Carbon::now(),
                'phone' => $user['phone'],
                'address' => $user['address'],
                'password' => Hash::make(123123123)
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->users as $user) {
            User::where('email', $user['email'])->delete();
        }
    }
}
