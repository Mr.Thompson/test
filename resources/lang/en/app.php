<?php

return [

    'buttons' => [
        'add' => 'Add',
        'cancel' => 'Cancel',
        'read_more' => 'Read more'
    ],

    'user' => [
        'title' => 'Users',
        'columns' => [
            'id' => 'Id',
            'isActive' => 'Active',
            'age' => 'Age',
            'eyeColor' => 'Eye Color',
            'name' => 'Name',
            'gender' => 'Gender',
            'company' => 'Company',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'password' => 'Password',
            'email_verified_at' => 'Email_verified_at',
            'remember_token' => 'Remember_token',
            'created_at' => 'Created_at',
            'updated_at' => 'Updated_at',
            'json' => 'Json'
        ]
    ],

];
