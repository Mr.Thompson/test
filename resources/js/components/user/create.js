Vue.component('user-create', {
    data() {
        return {
            json: {},
            link: '/users',
        }
    },
    props: {
        data: {
            default: () => {
                return {}
            }
        }
    },
    methods: {
        store() {
            console.log(this.json.data);
            axios.post(this.link, this.json.data)
                .then(response => {
                    window.location.href = this.link;
                });
        }
    }
});
