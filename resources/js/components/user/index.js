Vue.component('user-index', {
    data() {
        return {
            data: {
                users: {},
                active: true,
                column: 'id',
                more: false,
                order: 'asc'
            },
            user_id: null,
            link: '/users',
        }
    },
    mounted() {
        this.data.users = this.users;
    },
    props: {
        users : {
            default: () => {
                return []
            }
        }
    },
    methods: {
        read_more() {
            console.log(this.data);
            this.data.more = true;
            axios.get('/users', {
                params: this.data
            })
                .then(response => {
                    this.data = response.data
                })
        },
        order_by(column, order) {
            console.log(this.data);
            this.data.column = column;
            this.data.order = this.data.order == order ? 'asc' : 'desc';
            axios.get('/users', {
                params: this.data
            })
                .then(response => {
                    this.data = response.data
                })
        }
    },
});
