@extends('layouts.app')

@section('content')
<user-index
    :users="{{collect($users)->toJson() }}"
    inline-template>
    <div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">
                    <div>
                        @lang('app.user.title')
                    </div>
                </h3>
            </div>
        </div>
        <div class="content-body">
            <section id="search-website"
                     class="card overflow-hidden"
            >
                <div class="card-content collapse show">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="text-right">
                                <div class="text-left text-sm-right px-0 py-0 mb-1"
                                     id="headingBTwo"
                                >
                                    <a class="btn btn-success col-12 col-sm-auto"
                                       href="{{ route('users.create') }}"
                                    >
                                        <i class="fas fa-plus"></i>
                                        @lang('app.buttons.add')
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive mb-3">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="pointer"
                                            @click="order_by('id', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.id')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('age', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.age')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('eyeColor', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.eyeColor')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('name', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.name')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('gender', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.gender')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('company', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.company')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('email', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.email')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('phone', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.phone')
                                                </div>
                                            </div>
                                        </th>
                                        <th class="pointer"
                                            @click="order_by('address', 'desc')"
                                        >
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div>
                                                    @lang('app.user.columns.address')
                                                </div>
                                            </div>
                                        </th>

                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="user in data.users">
                                        <td>@{{ user.id }}</td>
                                        <td>@{{ user.age }}</td>
                                        <td>@{{ user.eyeColor }}</td>
                                        <td>@{{ user.name }}</td>
                                        <td>@{{ user.gender }}</td>
                                        <td>@{{ user.company }}</td>
                                        <td>@{{ user.email }}</td>
                                        <td>@{{ user.phone }}</td>
                                        <td>@{{ user.address }}</td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="button"
                                    class="btn btn-success"
                                    @click="read_more()"
                                    v-if="this.data.active"
                            >
                                @lang('app.buttons.read_more')
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</user-index>
@endsection
