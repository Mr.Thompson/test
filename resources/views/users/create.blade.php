@extends('layouts.app')

@section('content')
    <user-create inline-template>
        <div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">
                        @lang('app.buttons.add')
                    </h3>
                </div>
            </div>
            <div class="content-body">
                <section id="search-website"
                         class="card overflow-hidden"
                >
                    <div class="card-content collapse show mt-1">
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="form-group" >
                                        <label>
                                            @lang('app.user.columns.json')
                                        </label>
                                        <textarea
                                               v-model="json.data"
                                               class="form-control"
                                               placeholder="@lang('app.user.columns.json')"
                                        ></textarea>
                                    </div>
                                    <div class="form-actions text-center text-lg-right">
                                        <a class="btn btn-danger mb-1 mr-0 mr-lg-1"
                                           href="{{ route("users.index") }}"
                                        >
                                            <i class="ft-x"></i>
                                            @lang('app.buttons.cancel')
                                        </a>
                                        <button type="submit"
                                                class="btn btn-primary mb-1"
                                                @click="store"
                                        >
                                            <i class="la la-check-square-o"></i>
                                            @lang('app.buttons.add')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </user-create>
@endsection

